package com.example.contacttracing;

import java.io.Serializable;

public class Health_Info_Obj implements Serializable {
    Boolean withSymptoms;
    Boolean withContactHistory;

    public Health_Info_Obj(Boolean withSymptoms, Boolean withContactHistory) {
        this.withSymptoms = withSymptoms;
        this.withContactHistory = withContactHistory;
    }

    public Boolean getWithSymptoms() {
        return withSymptoms;
    }

    public void setWithSymptoms(Boolean withSymptoms) {
        this.withSymptoms = withSymptoms;
    }

    public Boolean getWithContactHistory() {
        return withContactHistory;
    }

    public void setWithContactHistory(Boolean withContactHistory) {
        this.withContactHistory = withContactHistory;
    }
}

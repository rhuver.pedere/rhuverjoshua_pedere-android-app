package com.example.contacttracing

import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.contacttracing.databinding.ActivityConfirmationBinding
import java.io.Serializable

class Confirmation : AppCompatActivity() {
    private lateinit var binding: ActivityConfirmationBinding
    private var listPersons: MutableList<Person> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConfirmationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init(){
        listPersons= intent.extras?.getSerializable("personList") as MutableList<Person>
        val personalInfo = intent.extras?.getSerializable("personalInfo") as Personal_Info_Obj
        val healthInfo = intent.extras?.getSerializable("healthInfo") as Health_Info_Obj
        Log.d(TAG,personalInfo.getFullName())
        Log.d(TAG, healthInfo.getWithSymptoms().toString())

        binding.tvFullName.text = personalInfo.getFullName()
        binding.tvMobileNumber.text = personalInfo.getMobileNumber()
        binding.tvCityResidence.text = personalInfo.getCityResidence()
        binding.switchSymptoms.isChecked = healthInfo.getWithSymptoms()
        binding.switchContacts.isChecked =healthInfo.getWithContactHistory()

        binding.confirm.setOnClickListener {
            val person = Person(personalInfo, healthInfo)
            listPersons.add(person)
            val intent = Intent(this, Acceptance::class.java)
            intent.putExtra("personList",listPersons as Serializable)
            startActivity(intent)
        }
    }

}
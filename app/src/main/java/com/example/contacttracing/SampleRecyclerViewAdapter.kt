package com.example.contacttracing

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.contacttracing.databinding.ItemPersonBinding

class SampleRecyclerViewAdapter(val list: MutableList<Person>) : RecyclerView.Adapter<SampleRecyclerViewAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding = ItemPersonBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(binding)
    }


//    override fun getItemCount() = l

    class MyViewHolder(val binding: ItemPersonBinding) : RecyclerView.ViewHolder(binding.root)

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.tvLabelFN.text = list[position].getPersonalInfo().getFullName()
        holder.binding.tvLabelMN.text = list[position].getPersonalInfo().getMobileNumber()
        holder.binding.tvLabelCR.text = list[position].getPersonalInfo().getCityResidence()
        holder.binding.switchSym.isChecked = list[position].getHealthInfo().getWithSymptoms()
        holder.binding.switchCon.isChecked = list[position].getHealthInfo().getWithContactHistory()
    }
}

package com.example.contacttracing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.contacttracing.databinding.ActivityGuestInfoBinding
import java.io.Serializable

class GuestInfo : AppCompatActivity() {

    private lateinit var binding: ActivityGuestInfoBinding
    private var listPersons: MutableList<Person> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGuestInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init(){
        listPersons= intent.extras?.getSerializable("personList") as MutableList<Person>
        binding.next.setOnClickListener {
            val personalInfo = Personal_Info_Obj(
                binding.etFullName.text.toString(),
                binding.etMobileNumber.text.toString(),
                binding.etCityResidence.text.toString()
            )



            val intent = Intent(this, HealthInfo::class.java)
            intent.putExtra("personalInfo", personalInfo as Serializable)
            intent.putExtra("personList",listPersons as Serializable)
            startActivity(intent)

        }
    }
}
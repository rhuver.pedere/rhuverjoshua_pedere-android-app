package com.example.contacttracing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.contacttracing.databinding.ActivityAcceptanceBinding
import com.example.contacttracing.databinding.ActivityConfirmationBinding
import java.io.Serializable

class Acceptance : AppCompatActivity() {
    private lateinit var binding: ActivityAcceptanceBinding
    private var listPersons: MutableList<Person> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAcceptanceBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init(){
        listPersons= intent.extras?.getSerializable("personList") as MutableList<Person>

        binding.goToHome.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("personList",listPersons as Serializable)
            startActivity(intent)
        }
    }
}
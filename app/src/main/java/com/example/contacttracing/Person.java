package com.example.contacttracing;

import java.io.Serializable;

public class Person implements Serializable {
    Personal_Info_Obj personalInfo;
    Health_Info_Obj healthInfo;

    public Person(Personal_Info_Obj personalInfo, Health_Info_Obj healthInfo) {
        this.personalInfo = personalInfo;
        this.healthInfo = healthInfo;
    }

    public Personal_Info_Obj getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(Personal_Info_Obj personalInfo) {
        this.personalInfo = personalInfo;
    }

    public Health_Info_Obj getHealthInfo() {
        return healthInfo;
    }

    public void setHealthInfo(Health_Info_Obj healthInfo) {
        this.healthInfo = healthInfo;
    }
}

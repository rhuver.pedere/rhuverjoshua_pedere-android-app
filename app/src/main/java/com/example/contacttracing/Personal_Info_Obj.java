package com.example.contacttracing;

import java.io.Serializable;

public class Personal_Info_Obj implements Serializable {
    String fullName;
    String mobileNumber;
    String cityResidence;

    public Personal_Info_Obj(String fullName, String mobileNumber, String cityResidence) {
        this.fullName = fullName;
        this.mobileNumber = mobileNumber;
        this.cityResidence = cityResidence;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getCityResidence() {
        return cityResidence;
    }

    public void setCityResidence(String cityResidence) {
        this.cityResidence = cityResidence;
    }
}

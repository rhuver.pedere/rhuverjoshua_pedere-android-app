package com.example.contacttracing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.contacttracing.databinding.ActivityHealthInfoBinding
import java.io.Serializable

class HealthInfo : AppCompatActivity() {
    private lateinit var binding: ActivityHealthInfoBinding
    private var listPersons: MutableList<Person> = mutableListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHealthInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }
    private fun init(){
        listPersons= intent.extras?.getSerializable("personList") as MutableList<Person>
        val personalInfo = intent.extras?.getSerializable("personalInfo") as Personal_Info_Obj



        binding.next.setOnClickListener {
            val healthInfo = Health_Info_Obj(
                binding.switchSymptoms.isChecked,
                binding.switchContacts.isChecked

            )



            val intent = Intent(this, Confirmation::class.java)
            intent.putExtra("personalInfo", personalInfo as Serializable)
            intent.putExtra("healthInfo", healthInfo as Serializable)
            intent.putExtra("personList",listPersons as Serializable)
            startActivity(intent)

        }
    }
}
package com.example.contacttracing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.contacttracing.databinding.ActivityMainBinding
import java.io.Serializable

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var listPersons: MutableList<Person> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        init()


    }

    private fun init(){

        var z = intent.hasExtra("personList")


        if(z) {

            listPersons = intent.extras?.getSerializable("personList") as MutableList<Person>
            binding.personListRCView.adapter?.notifyDataSetChanged()

        }

        setUpRecyclerView(listPersons)

        binding.addPersonBtn.setOnClickListener {
            val intent = Intent(this, GuestInfo::class.java)
            intent.putExtra("personList",listPersons as Serializable)
            startActivity(intent)
        }
    }

    private fun setUpRecyclerView(listPersons: MutableList<Person>) {

        binding.personListRCView.apply {
            adapter = SampleRecyclerViewAdapter(list = listPersons)
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

    }

}